# Find the number
A simple game to find a random number between 0 and 100. Just run this command to start the game :
```sh
cargo r
```