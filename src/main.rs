use find_number::game::FindTheNumber;

fn main() {
    let mut find_number = FindTheNumber::new();
    find_number.start();
}
