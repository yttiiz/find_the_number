pub mod game {
    use rand::prelude::*;
    use std::io;

    pub struct FindTheNumber {
        number_to_find: u8,
        number_of_try: u8,
        count: u8,
    }

    impl FindTheNumber {
        pub fn new() -> Self {
            Self {
                number_to_find: thread_rng().gen_range(0..100),
                number_of_try: 10,
                count: 0,
            }
        }

        pub fn start(&mut self) -> () {
            println!("********************************************");
            println!("YOU HAVE TO GUESS A NUMBER BETWEEN 0 AND 100");
            println!("********************************************");
            println!("Generate number...");
            println!("Let's go !!!! You got 10 trys.");

            while self.number_of_try > 0 {
                let mut entry = String::new();

                match self.count {
                    0 => println!("Enter a number !"),
                    _ => println!("Enter a new number !"),
                }

                io::stdin()
                    .read_line(&mut entry)
                    .expect("You have to give a valid entry.");

                let number = entry.as_str().trim().parse::<u8>().unwrap_or_else(|_| {
                    println!("You have to enter a number.");
                    0
                });

                match self.checker(&number) {
                    false => {
                        self.count += 1;
                        self.number_of_try -= 1;
                        self.remaining_try();

                        if let 0 = self.number_of_try {
                            println!("Game over !!! The guess number was {}", self.number_to_find)
                        }
                    }
                    true => {
                        println!(
                            "You find the right number {} after {} try(s).",
                            self.number_to_find,
                            self.count + 1
                        );
                        return;
                    }
                };
            }
        }

        fn checker(&self, num: &u8) -> bool {
            println!("***********************************");

            match self.number_to_find < *num || self.number_to_find > *num {
                true => {
                    self.print_message(*num);
                    false
                }
                _ => true,
            }
        }

        fn print_message(&self, num: u8) -> () {
            let height = if self.number_to_find > num {
                "small"
            } else {
                "high"
            };
            println!("Your number {num} is too {height}");
        }

        fn remaining_try(&self) -> () {
            println!("You have {} attempt(s) left.", self.number_of_try);
        }
    }
}
